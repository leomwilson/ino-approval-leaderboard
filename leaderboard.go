package main

import (
  "fmt"
  "flag"
  "sort"
  "encoding/json"
  "io/ioutil"
)

type Instant struct {
  Entities map[string]Entity
}
type Entity struct {
  Lvl float64
  Name string
  Pubkey string
  Privkey string
  Sessions []Session
}
type Session struct {
  Start float64
  End float64
  Location string
  Valid bool
  Approver string
}

type Entry struct {
  Id string
  Count int16
}

func main() {
  var fname string
  var scanuser string
  
  flag.StringVar(&fname, "i", "data.json", "Input file containing JSON data from the server")
  flag.StringVar(&scanuser, "u", "", "The character to track")

  flag.Parse()

  if scanuser == "" {
    panic("Please provide a character EID to track")
  }

  f, err := ioutil.ReadFile(fname)
  if err != nil {
    panic(err)
  }

  counter := make(map[string]int16)

  names := make(map[string]string)

  var d Instant
  if err := json.Unmarshal(f, &d); err != nil {
    panic(err)
  }

  for k := range d.Entities {
    names[k] = d.Entities[k].Name
    if k == scanuser {
      for s := range d.Entities[k].Sessions {
        approver := d.Entities[k].Sessions[s].Approver
        if approver != "" {
          counter[approver] ++
        }
      }
    }
  }

  sortable := make([]Entry, 0)

  for approver := range counter {
    sortable = append(sortable, Entry{approver, counter[approver]})
  }

  sort.Slice(sortable, func(i, j int) bool {
    return sortable[i].Count > sortable[j].Count
  })

  for _, entry := range sortable {
    fmt.Printf("%s (%d)\n", names[entry.Id], entry.Count)
  }
}
